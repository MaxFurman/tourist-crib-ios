//
//  ViewController.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/5/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import UIKit
import CoreLocation
import SystemConfiguration
import Toast_Swift

class FilterVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, CLLocationManagerDelegate {

    var picker: UIPickerView!
    
    @IBOutlet weak var textFieldPrice: UITextField!
    @IBOutlet weak var textFieldDistrict: UITextField!
    @IBOutlet weak var textFieldType: UITextField!
    
    var activeTextField: UITextField!
    var selectionsArray: [String]!
    var filterResults: [Int]!
    var filterIndex: Int!
    let locationManager: CLLocationManager = CLLocationManager()
    
    var launchedWithInternet: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        
        filterResults = [0,0,0]
        
        setPickerView()
        
        if connectedToNetwork() {
            print("Connected to network")
            launchedWithInternet = true
            getFirebaseData()
        } else {
            makeInternetToast()
            print("Doesn't connected to network")
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        locationAuth()
    }
    
    //Check internet connection
    func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    //Show internet required toast
    func makeInternetToast(){
        self.view.makeToast("Отсутствует интернет соединение! Подключитесь к сети и перезапустите приложение!", duration: 4.0, position: .bottom)
    }
    
    
    //Location authorization
    func locationAuth(){
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            
        } else {
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    func setPickerView(){
        picker = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.white
        textFieldPrice.inputView = picker
        textFieldDistrict.inputView = picker
        textFieldType.inputView = picker
        textFieldPrice.delegate = self
        textFieldDistrict.delegate = self
        textFieldType.delegate = self
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        textFieldPrice.inputAccessoryView = toolBar
        textFieldDistrict.inputAccessoryView = toolBar
        textFieldType.inputAccessoryView = toolBar
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return selectionsArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return selectionsArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.activeTextField.text = selectionsArray[row]
        filterResults[filterIndex] = row
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //Configure picker for certain textField
        switch textField {
        case textFieldPrice:
            selectionsArray = PLACES_PRICE_RANGE
            filterIndex = 0
        case textFieldDistrict:
            selectionsArray = PLACES_DISTRICT
            filterIndex = 1
        case textFieldType:
            selectionsArray = PLACES_TYPE
            filterIndex = 2
        default:
            break
        }
        picker.reloadAllComponents()
        picker.selectRow(filterResults[filterIndex], inComponent: 0, animated: false)
        activeTextField = textField
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("\(String(describing: textField.text!))")
    }
    
    @objc func doneClick(){
        activeTextField.resignFirstResponder()
    }
    
    func cancelClick(){
        activeTextField.resignFirstResponder()
    }
    
    
    @IBAction func searchBtnPressed(_ sender: Any) {
        if launchedWithInternet {
            performSegue(withIdentifier: "TableVC", sender: filterResults)
        } else {
            makeInternetToast()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TableVC"{
            if let navVC = segue.destination as? UINavigationController, let tableVC = navVC.viewControllers.first as? TableVC{
                if let sender = sender as? [Int]{
                    tableVC.filters = sender
                }
            }
        }
    }
    
    //get places from Firebase
    func getFirebaseData(){
        DataService.instance.placesRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if let places = snapshot.value as? Dictionary<String,AnyObject>{
                for place in places{
                    if let value = place.value as? Dictionary<String,AnyObject>{
                        let myPlace = MyPlace(uuid: place.key, place: value)
                        PlacesSingleton.instance.addPlace(myPlace: myPlace)
                    }
                }
            }
        })
    }
}





