//
//  RoundView.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/12/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import UIKit

class RoundView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 10.0
    }

}
