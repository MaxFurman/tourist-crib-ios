//
//  CustomInfoWindow.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/20/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import UIKit
import Cosmos
import GooglePlaces

class CustomInfoWindow: UIView {

    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var ratingBar: CosmosView!
    @IBOutlet weak var distanceIcon: UIImageView!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    

    func configureInfoWindow(myPlace: MyPlace){
        nameLbl.text = myPlace.placeName
        ratingBar.rating = myPlace.placeRating
        detailsLbl.text = myPlace.placeDetails
        
        
        guard let distanceData = myPlace.placeDistance else {
            self.distanceIcon.isHidden = true
            self.distanceLbl.text = ""
            return
        }
        distanceIcon.isHidden = false
        if distanceData < 1000.0 {
            distanceLbl.text = "\(Int(distanceData))м"
        } else if distanceData >= 100000.0 {
            distanceLbl.text = ">1000км"
        } else {
            distanceLbl.text = "\(round(distanceData)/1000)км"
        }
    }

}
