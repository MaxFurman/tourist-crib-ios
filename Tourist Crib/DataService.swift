//
//  DataService.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/12/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import Foundation
import Firebase

class DataService{
    private static let _dataService: DataService = DataService()
    
    static var instance: DataService{
        return _dataService
    }
    
    var mainRef: DatabaseReference{
        return Database.database().reference()
    }
    
    var placesRef: DatabaseReference{
        return mainRef.child(FIR_PLACES)
    }
    

}
