//
//  PlaceCell.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/7/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import UIKit
import Cosmos
import GooglePlaces

class PlaceCell: UITableViewCell {
    
    
    @IBOutlet weak var placeImg: UIImageView!
    @IBOutlet weak var placeNameLbl: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var distanceIcon: UIImageView!
    
    var placeId : String!
    var place : MyPlace!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 5.0
        ratingView.settings.fillMode = .half
        

    }
    
    func configureCell(myPlace: MyPlace) {
        place = myPlace
        placeNameLbl.text = myPlace.placeName
        ratingView.rating = myPlace.placeRating
        details.text = myPlace.placeDetails
        placeId = myPlace.placeId
        placeImg.image = place.photo ?? UIImage(named: "no_photo.jpg")

        guard let distanceData = myPlace.placeDistance else {
            self.distanceIcon.isHidden = true
            self.distanceLbl.text = ""
            return
        }
        distanceIcon.isHidden = false
        if distanceData < 1000.0 {
            distanceLbl.text = "\(Int(distanceData))м"
        } else if distanceData >= 100000.0 {
            distanceLbl.text = ">1000км"
        } else {
            distanceLbl.text = "\(round(distanceData)/1000)км"
        }
        
    }
    


}
