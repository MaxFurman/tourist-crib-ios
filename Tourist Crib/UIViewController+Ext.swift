//
//  UIViewController+Ext.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/15/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import UIKit

extension UIViewController{
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
}
