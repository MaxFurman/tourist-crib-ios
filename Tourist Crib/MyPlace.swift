//
//  Place.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/13/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import UIKit

class MyPlace{
    private var _uuid: String!
    private var _placeId: String!
    private var _placeName: String!
    private var _placeAddress: String!
    private var _placeDetails: String!
    
    private var _placeRating: Double!
    
    private var _longitude: Double!
    private var _latitude: Double!
    
    private var _priceIndex: Int!
    private var _districtIndex: Int!
    private var _typeIndex: Int!
    
    private var _placeDistance: Float?
    
    
    var uuid: String{
        return _uuid
    }
    
    var placeId: String{
        set{
          _placeId = newValue
        } get{
            return _placeId
        }
    }
    
    var placeName: String{
        set{
          _placeName = newValue
        } get{
            return _placeName
        }
    }
    
    var placeAddress: String{
        return _placeAddress
    }
    
    var placeDetails: String{
        return _placeDetails
    }
    
    var placeRating: Double{
        return _placeRating
    }
    
    var lalitude: Double{
        return _latitude
    }
    
    var longitude: Double{
        return _longitude
    }
    
    var priceIndex: Int{
        return _priceIndex
    }
    
    var districtIndex: Int{
        return _districtIndex
    }
    
    var typeIndex: Int{
        return _typeIndex
    }
    
    var placeDistance: Float?{
        get{
          return _placeDistance
        } set{
            _placeDistance = newValue
        }
    }
    
    var photo: UIImage?

    
    init(uuid: String, placeId: String, placeName: String, placeAddress: String, placeDetails: String, latitude: Double, longitude: Double,
         placeRating: Double, priceIndex: Int, districtIndex: Int, typeIndex: Int) {
        _uuid = uuid
        _placeId = placeId
        _placeName = placeName
        _placeAddress = placeAddress
        _placeDetails = placeDetails
        _latitude = latitude
        _longitude = longitude
        _placeRating = placeRating
        _priceIndex = priceIndex
        _districtIndex = districtIndex
        _typeIndex = typeIndex
    }
    
    init(uuid: String ,place: Dictionary<String,AnyObject>) {
        _uuid = uuid
        _placeId = place["placeId"] as! String
        _placeName = place["placeName"] as! String
        _placeAddress = place["placeAddress"] as! String
        _placeDetails = place["placeDetails"] as! String
        _latitude = place["latitude"] as! Double
        _longitude = place["longitude"] as! Double
        _placeRating = place["placeRating"] as! Double
        _priceIndex = place["priceIndex"] as! Int
        _districtIndex = place["districtIndex"] as! Int
        _typeIndex = place["typeIndex"] as! Int
    }
}


