//
//  GooglePlaceService.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/17/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import Foundation
import GooglePlaces

class GooglePlaceService{
    
    private static let _instance: GooglePlaceService = GooglePlaceService()
    
    static var instance : GooglePlaceService{
        return _instance
    }
    
    
    func loadFirstPhotoForPlace(myPlace: MyPlace, tableView: UITableView) {
        GMSPlacesClient.shared().lookUpPhotos(forPlaceID: myPlace.placeId) { (photos, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                if let firstPhoto = photos?.results.first {
                    self.loadImageForMetadata(photoMetadata: firstPhoto, place: myPlace, tableView: tableView)
                }
            }
        }
    }
    
    
    func loadImageForMetadata(photoMetadata: GMSPlacePhotoMetadata, place: MyPlace, tableView: UITableView) {
        GMSPlacesClient.shared().loadPlacePhoto(photoMetadata, constrainedTo: CGSize(width: 107, height: 107), scale: 1.0) { (photo, error) in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                place.photo = photo
                tableView.reloadData()
            }
        }
    }
    
    
}
