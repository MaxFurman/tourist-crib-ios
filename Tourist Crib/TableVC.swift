//
//  TableVC.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/7/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import UIKit
import Firebase
import GooglePlaces
import CoreLocation
import SwiftPullToRefresh


class TableVC: UIViewController, UINavigationControllerDelegate, UISearchBarDelegate, CLLocationManagerDelegate  {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    let locationManager = CLLocationManager()
    var filters: [Int]!
    var placesList: [MyPlace] = [MyPlace]()
    var filteredList: [MyPlace] = [MyPlace]()
    var placesToLoad: [MyPlace] = [MyPlace]()
    var inSearchMode = false
    var currentLocation: CLLocation?
    var loadMoreIndex: Int = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        checkLocationPermission()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.spr_setTextFooter {
            // do your action here
            let listCount: Int = self.inSearchMode ? self.filteredList.count : self.placesList.count
            if self.loadMoreIndex < listCount {
                self.getMoreData()
            }
            self.tableView.spr_endRefreshing()
        }
        
        
        searchBar.delegate = self
        
        //Hide keyboard by clicking out of keyboard. Extension.
        hideKeyboard()
        
        searchBar.returnKeyType = .done
        
        loadMoreIndex = 0
        getData()
        
    }

    //Try to get location coordinates
    func checkLocationPermission(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            currentLocation = locationManager.location
        }
    }


    @IBAction func filterBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func getData(){
        let places: [MyPlace] = PlacesSingleton.instance.places
        print(places.count)
        for place in places{
            //Filter places by parameters and reload data
            self.filterPlace(myPlace: place)
            
        }
        self.sortPlaces()
        self.loadPhotos()
        self.getMoreData()
        self.tableView.reloadData()
    }
    
    func getMoreData(){

        let listCount: Int = inSearchMode ? filteredList.count : placesList.count
        for _ in 1...5{
            
            if loadMoreIndex >= listCount{
                break
            }
            loadMoreIndex += 1
        }
        self.loadPhotos()
        self.tableView.reloadData()
    }
    
    //Filter place and add it to places list if succeded
    func filterPlace(myPlace: MyPlace){
        let placeFilters: [Int] = [myPlace.priceIndex, myPlace.districtIndex, myPlace.typeIndex]
        guard placeFilters[0] == self.filters[0] || self.filters[0] == 0 else {
            return
        }
        guard placeFilters[1] == self.filters[1] || self.filters[1] == 0 else {
            return
        }
        
        guard placeFilters[2] == self.filters[2] || self.filters[2] == 0 else {
            return
        }
        self.placesList.append(myPlace)
    }
    
    //Sort places by the nearest if user location is permitted or by place rating if denied.
    func sortPlaces(){
        if currentLocation == nil {
            placesList = placesList.sorted(by: { $0.placeRating > $1.placeRating })
        } else {
            for place in placesList{
                let placeLocation: CLLocation = CLLocation(latitude: place.lalitude, longitude: place.longitude)
                let distance: Float = Float(currentLocation!.distance(from: placeLocation))
                place.placeDistance = round(distance)
            }
            placesList = placesList.sorted(by: {$0.placeDistance! < $1.placeDistance!})
        }
    }
    
    //Load photos for every place from Google Places API
    func loadPhotos(){
        for place in placesList{
            GooglePlaceService.instance.loadFirstPhotoForPlace(myPlace: place, tableView: tableView)
        }
    }
    
    
    //Search bar config
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let indexPath = IndexPath(row: 0, section: 0)
        if tableView.numberOfRows(inSection: 0) != 0 {
            self.tableView.scrollToRow(at: indexPath, at: .none, animated: true)
        }
        if searchBar.text == nil || searchBar.text == ""{
            inSearchMode = false
            searchBar.endEditing(true)
            self.tableView.reloadData()
        } else {
            inSearchMode = true
            let lowerTxt: String = searchBar.text!.lowercased()
            filteredList = placesList.filter({$0.placeName.lowercased().contains(lowerTxt)})
            self.tableView.reloadData()
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlaceDetailsVC"{
            if let destination = segue.destination as? PlaceDetailsVC{
                if let sender = sender as? MyPlace{
                    destination.myPlace = sender
                }
            }
        }
    }
    
}


extension TableVC: UITableViewDelegate, UITableViewDataSource {
    //Table view config
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inSearchMode{
            return filteredList.count
        }else {
            return loadMoreIndex
        } 
    } 
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell", for: indexPath) as? PlaceCell{
            let myPlace: MyPlace!
            if inSearchMode{
                myPlace = filteredList[indexPath.row]
            }else {
                print(loadMoreIndex)
                myPlace = placesList[indexPath.row]
            }
            cell.configureCell(myPlace: myPlace)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedPlace: MyPlace!
        if inSearchMode{
            selectedPlace = filteredList[indexPath.row]
        }else {
            selectedPlace = placesList[indexPath.row]
        }
        performSegue(withIdentifier: "PlaceDetailsVC", sender: selectedPlace)
    }
}



