//
//  PlaceDetailsVC.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/7/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import UIKit
import Cosmos
import FSPagerView
import GooglePlaces

class PlaceDetailsVC: UIViewController, FSPagerViewDelegate, FSPagerViewDataSource{
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet{
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            
        }
    }
    @IBOutlet weak var pagerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var pageControl: FSPageControl!{
        didSet{
            self.pageControl.numberOfPages = 5
            self.pageControl.contentHorizontalAlignment = .right
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    
    var myPlace: MyPlace!
    let noPhotoImg: UIImage = UIImage(named: "no_photo.jpg")!
    var photosArray: [UIImage] = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        configurePageView()
        
        setPlaceData()
        
        loadPhotosForArray(placeID: myPlace.placeId)
        
        
    }
    
    //Set place data
    func setPlaceData(){
        nameLbl.text = myPlace.placeName
        addressLbl.text = myPlace.placeAddress
        detailsLbl.text = "Цена \(PLACES_PRICE_RANGE[myPlace.priceIndex]), \(PLACES_DISTRICT[myPlace.districtIndex]) район, \(PLACES_TYPE[myPlace.typeIndex])\n\(myPlace.placeDetails)"
        ratingView.settings.fillMode = .half
        ratingView.rating = myPlace.placeRating
    }
    
    
    //Pager view configs
    
    func configurePageView(){
        pagerView.delegate = self
        pagerView.dataSource = self
        let navHeight: CGFloat!
        if let height = self.navigationController?.navigationBar.frame.height {
            navHeight = height
        } else {
            navHeight = 0
        }
        pagerViewHeightConstraint.constant = (self.view.frame.height - navHeight) / 2
        pageControl.currentPage = 0
        pageControl.setFillColor(.lightGray, for: .normal)
        pageControl.setFillColor(.white, for: .selected)
    }
    

    
    func numberOfItems(in pagerView: FSPagerView) -> Int{
        return 5
    }
    
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell: FSPagerViewCell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        if cell.imageView?.image == nil {
            cell.imageView?.image = noPhotoImg
        }
        

        if index < photosArray.count {
            cell.imageView?.image = photosArray[index]
        }
        
        
        cell.imageView?.contentMode = .scaleAspectFill
        return cell
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard self.pageControl.currentPage != pagerView.currentIndex else {
            return
        }
        self.pageControl.currentPage = pagerView.currentIndex
    }
    

    
    func pagerView(_ pagerView: FSPagerView, shouldHighlightItemAt index: Int) -> Bool {
        return false
    }

    func pagerView(_ pagerView: FSPagerView, shouldSelectItemAt index: Int) -> Bool {
        return false
    }
    
    //Load photos for pager view
    func loadPhotosForArray(placeID: String) {
        GMSPlacesClient.shared().lookUpPhotos(forPlaceID: placeID) { (photos, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                guard let photos = photos else {
                    return
                }
                var index = 0
                while index < photos.results.count && index < 6 && photos.results.count != 0{
                    let certainPhoto = photos.results[index]
                    self.loadImageForArray(photoMetadata: certainPhoto, index: index)
                    index += 1
                }
                
                
                
            }
        }
    }
    
    func loadImageForArray(photoMetadata: GMSPlacePhotoMetadata, index: Int) {
        GMSPlacesClient.shared().loadPlacePhoto(photoMetadata, constrainedTo: CGSize(width: self.pagerView.frame.width, height: self.pagerView.frame.height), scale: 1.0) { (photo, error) in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                self.photosArray.append(photo!)
                self.pagerView.reloadData()
            }
        }
    }
    

    //Perform map segue
    
    @IBAction func mapBtnPressed(sender: UIButton){
        performSegue(withIdentifier: "MapVC", sender: myPlace)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MapVC"{
            if let destination = segue.destination as? MapVC{
                if let sender = sender as? MyPlace{
                    destination.myPlace = sender
                }
            }
        }
    }
    
}
