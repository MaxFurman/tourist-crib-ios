//
//  RoundButton.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/12/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import UIKit

class RoundButton: UIButton {

    override func awakeFromNib() {
        layer.cornerRadius = 6.0
        layer.backgroundColor = UIColor.white.cgColor
        let customOrange = UIColor(red: 255/255.0, green: 152/255.0, blue: 0/255.0, alpha: 1.0)
        setTitleColor(customOrange, for: .normal)
    }

}
