//
//  Constants.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/7/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import Foundation

let FIR_PLACES = "places"

let PLACES_PRICE_RANGE = [
    "Любой",
    "Бесплатно",
    "До 100 гривен",
    "100-250 гривен",
    "250-500 гривен",
    "500-1000 гривен",
    "1000+ гривен"
]

let PLACES_DISTRICT = [
    "Любой",
    "Голосеевский",
    "Дарницкий",
    "Деснянский",
    "Днепровский",
    "Оболонский",
    "Печерский",
    "Подольский",
    "Святошинский",
    "Соломенский",
    "Шевченковский"
]

let PLACES_TYPE = [
    "Любой",
    "Кафе",
    "Клуб",
    "Кондитерская",
    "Кофейня",
    "Паб",
    "Пиццерия",
    "Ресторан",
    "Суши",
    "Фастфуд"
]
