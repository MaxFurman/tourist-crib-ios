//
//  PlacesSingleton.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/24/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import Foundation


class PlacesSingleton {
    private static let _instance: PlacesSingleton = PlacesSingleton()
    
    static var instance: PlacesSingleton{
        return _instance
    }
    
    private var _places: [MyPlace]!
    
    var places: [MyPlace]{
        return _places
    }
    
    init() {
        _places = [MyPlace]()
    }
    
    
    func addPlace(myPlace: MyPlace){
        _places.append(myPlace)
    }
}
