//
//  MapVC.swift
//  Tourist Crib
//
//  Created by Max Furman on 8/7/17.
//  Copyright © 2017 Max Furman. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Toast_Swift

class MapVC: UIViewController, GMSMapViewDelegate {
    

    @IBOutlet weak var mapView: GMSMapView!
    
    var placesClient: GMSPlacesClient!
    let marker: GMSMarker = GMSMarker()
    
    
    var myPlace: MyPlace!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
        mapView.settings.compassButton = true
        let camera = GMSCameraPosition.camera(withLatitude: myPlace.lalitude, longitude: myPlace.longitude, zoom: 15.0)
        mapView.camera = camera
        //Set a marker in the center of the map.
        marker.position = CLLocationCoordinate2D(latitude: myPlace.lalitude, longitude: myPlace.longitude)
        marker.map = mapView
        mapView.selectedMarker = marker
        
        
    }
    
    
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let infoWindow: CustomInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)?.first as! CustomInfoWindow
        infoWindow.configureInfoWindow(myPlace: myPlace)
        marker.tracksInfoWindowChanges = true
        loadFirstPhotoForPlace(placeID: myPlace.placeId, placeImgView: infoWindow.photoView)
        return infoWindow
    }
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        let searchText: String = "comgooglemaps://?q=\(myPlace.placeName),\(myPlace.placeAddress)&center=\(myPlace.lalitude),\(myPlace.longitude)"
            .replacingOccurrences(of: " ", with: "+")
            .addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        print(searchText)
        let googleMapsUrl: URL = URL(string: searchText)!
        UIApplication.shared.open(googleMapsUrl, options: [:]) { (success) in
            if !success{
                let appleSearchText: String = "http://maps.apple.com/?q=\(self.myPlace.placeName)&sll=\(self.myPlace.lalitude),\(self.myPlace.longitude)&z=10&t=s"
                    .replacingOccurrences(of: " ", with: "+")
                    .addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
                let appleMapsUrl: URL = URL(string: appleSearchText)!
                UIApplication.shared.open(appleMapsUrl, options: [:], completionHandler: { (success) in
                    if !success{
                        self.view.makeToast("К сожалению, не получается открыть карту! Установите Google Maps или Apple maps и попытайтесь снова!", duration: 4.0, position: .bottom)
                    }
                })
            }
        }
 
    }
    



    //Load photo
    func loadFirstPhotoForPlace(placeID: String, placeImgView: UIImageView) {
        GMSPlacesClient.shared().lookUpPhotos(forPlaceID: placeID) { (photos, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                if let firstPhoto: GMSPlacePhotoMetadata = photos?.results.first {
                    self.loadImageForMetadata(photoMetadata: firstPhoto, placeImgView: placeImgView)
                }
            }
        }
    }
    
    func loadImageForMetadata(photoMetadata: GMSPlacePhotoMetadata, placeImgView: UIImageView) {
        GMSPlacesClient.shared().loadPlacePhoto(photoMetadata, callback: {
            (photo, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                placeImgView.image = photo
            }
            self.marker.tracksInfoWindowChanges = false
        })
    }

}
